
$(document).ready(function(){
    // invoke the carousel
        $('#homecarousel').carousel({
          interval: 3000
        });
    
    // scroll slides on mouse scroll 
    $('#homecarousel').bind('mousewheel DOMMouseScroll', function(e){
    
            if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
                $(this).carousel('prev');
                
            }
            else{
                $(this).carousel('next');
                
            }
        });
    
    //scroll slides on swipe for touch enabled devices 
    
         $("#homecarousel").on("touchstart", function(event){
     
            var yClick = event.originalEvent.touches[0].pageY;
            $(this).one("touchmove", function(event){
    
            var yMove = event.originalEvent.touches[0].pageY;
            if( Math.floor(yClick - yMove) > 1 ){
                $(".carousel").carousel('next');
            }
            else if( Math.floor(yClick - yMove) < -1 ){
                $(".carousel").carousel('prev');
            }
        });
        $(".carousel").on("touchend", function(){
                $(this).off("touchmove");
        });
    });
        
    });


    /* ========================================================== */

	// Fix column sibling height 
	function fixHeight() {
		$(".data-height-fix").each(function() {
			var $this = $(this),
				siblingHeight = $this.find($(".get-height")).outerHeight();
			$this.find(".set-height").css("height",siblingHeight);
		});
	}

	$(window).on('load', function() {
		fixHeight();
	});




    /* ========================================================== */
    // simulate a hover with a touch in touch enabled browsers

    $(document).ready(function() {
        $('.hover').on('touchstart', function (e) {
            'use strict'; //satisfy code inspectors
            var link = $(this); //preselect the link
            if (link.hasClass('hover_effect')) {
                return true;
            } else {
                link.addClass('hover_effect');
                $('.hover').not(this).removeClass('hover_effect');
                e.preventDefault();
                return false; //extra, and to make sure the function has consistent return points
            }
        });
    });