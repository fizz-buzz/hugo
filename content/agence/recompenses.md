---
title: Récompenses
date: 2018-09-13 20:51:27 +0000
layout: recompenses
weight: 30
client: clients/jean-stalaven-1.md

---
Mappic Best Retail Store Design 2018  
_Franprix Darwin_

Janus du commerce 2018  
_Nouveau concept Petit Casino_

Club des directeur artistique 2017
_Création de caractère pour Les Toqués du Pain_

A design Award 2017
_Concept Les Toqués du Pain  
Packaging Sèches pleurs Monoprix_

Club des directeur artistique 2016
_Création de caractère pour Petit Casino_

Janus du commerce 2016
_Nouveau concept Franprix_

Grand prix Stratégie du Design 2015
_Prix packaging pour beaujolais nouveau 2014 Nicolas_

Club des Directeurs Artistiques 2014
_Prix Identité visuelle pour les Hang Tag Aigle_

International Food and Beverage & Creative Excellence Awards 2014
_Prix pour les Lupains_

Cristal Design 2013
_Prix Design pour le Beuajolias Nuovaeu Nicolas_

Grand Prix Stratégie du design 2013   
_Mention en design packaging pour les Virelangues Nicolas_

Super Design 2012
_Prix Design pour le Beuajolias Nuovaeu Nicolas 2012_

Grand Prix du Design Stratégie 2011   
_Prix Identité visuelle pour Les Lupains_

Club des Directeurs Artistiques 2011
_Prix Identité visuelle pour Les Lupains_

European Design Awards 2010
_Mention Monoprix Gamme Sèches Pleurs_

Super Design 2010
_Prix Design Packaging marque de distribution pour Monoprix Gamme Sèches Pleurs_

Top Com D’or 2010
_Monoprix Gamme Sèches Pleurs_

Club des Directeurs Artistiques 2010
_Mention Monoprix Gamme Sèches Pleurs_

Grand Prix Stratégie 2008   
_Mention Pack Monoprix Mono’pluie_