---
title: L’équipe
date: 2018-09-13 20:51:27 +0000
weight: 20
image1: "/uploads/equipe.jpg"
layout: equipe
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/equipe.jpg"
- template: ligne-6img-6img
  col6_img: []
aliases: 
  - /new/qui-sommes-nous/
  - /qui-sommes-nous/
---
### Un équipage à la Prévert…

#### 2 chats, 1 fondateur, 1 directeur de création, 2 directeurs de clientèle, 10 directeurs artistiques, 1 Sheila, 2 illustratrices, 1 concepteur rédacteur blagueur et 1 autre plus poète, plusieurs graphistes, 1 assistante, 1 cuisinier, 1 typographe...