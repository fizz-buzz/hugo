---
title: Histoire
date: 2018-09-13 20:51:27 +0000
weight: 5
aliases:
- "/une-histoire-pour-un-nom/"
client: clients/jean-stalaven-1.md

---
# Mon idéal était d’être classé "bon faiseur", expression rien moins que tapageuse dont j’apprécie ce qu’elle comporte d’honnêteté et de qualité.

### Christian Dior

## _

#### "Et pourtant, l’origine du nom des Bons Faiseurs ne vient pas de Christian Dior mais de _Maman Mimie_ ma grand-mère.

#### J’ai été élevé dans une famille nombreuse et trépidante, dans un petit village de Seine et Marne. À l'âge de 8 ans, mes grands-parents m'invitèrent à passer une semaine dans leur bel appartement haussmannien du Boulevard Raspail. Du haut de ma jeunesse et loin du cocon familial, ce moment fut riche de découvertes, notamment le merveilleux jardin du Luxembourg.

#### Mais de tous ces moments un détail de la vie quotidienne a marqué ma mémoire. À peine arrivé, ma grand-mère, élégamment habillée et munie de son cabas, m'embarquait dans _sa tournée des bons faiseurs_.

#### Intimidé, je la suivais pour un jeu de piste à la rencontre des petits commerçants du quartier.

#### Je finis par comprendre les subtilités de son cheminement. Il y avait le boucher qui _faisait bien_ le boeuf, et l’autre, _qui faisait bien_ le veau, le pâtissier qui faisait des délicieux éclairs au chocolat, et celui qui faisait une incomparable tarte aux pommes, le caviste qui avait les meilleurs vins, qui n’était pas avare de bons conseils, le droguiste de la rue Notre Dame aux Champs qui était plus cher que celui de la rue Vavin mais qui avait toujours la solution à ses problèmes ménagers. Ce rituel quotidien, se terminait par une rapide visite au Félix Potin, qu’elle avait l’air de fréquenter avec mépris pour acheter son anonyme lessive.

#### Je venais de découvrir qu'il y avait une alternative sensible et raffinée à la standardisation déshumanisée de l’hypermarché que mes parents fréquentaient exclusivement."

###### _

###### Étienne Rothé