---
title: Contact
date: 2018-09-13 20:51:27 +0000
weight: 40
image1: "/uploads/equipe.jpg"
layout: contact
aliases:
- nous-contacter
contact_email: contact@lesbonsfaiseurs.com
contact_adresse: 55 rue Meslay - 75003 PARIS / France
contact_phone: "+33 (0) 1 44 54 35 45"
aliases: 
    - /nous-contacter
---
Pour nous rencontrer, pour se voir en vrai, pour discuter un peu, pour partager un verre, pour parler projet ou chiffon, pour brainstormer ensemble, pour faire connaissance, pour faire la ribouldingue, pour un renseignement, pour se présenter, pour bavarder…