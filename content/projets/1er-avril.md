---
title: 1er Avril
client: clients/banette.md
date: 2018-11-15 14:36:12 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_05_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_03_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_04_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_07_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Operation_1er_Avril_08_2500x1650.jpg"
weight: 10

---
