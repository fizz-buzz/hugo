---
title: Banette identité
client: clients/banette.md
date: 2018-11-15 14:17:51 +0000
metiers:
- Packaging
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Identite_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_05_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_04_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_08_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Identite_03_2500x1650.jpg"
weight: ''
draft: true

---
