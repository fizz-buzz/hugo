---
title: Leader Price
client: clients/leader-price.md
date: 2018-11-16 15:06:16 +0000
metiers:
- Design global
- Retail
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Leader_Price_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x1650_10.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x1650_09-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x1650_08.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_04_2500x1650-2.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_04_960x1296-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_03_960x1296-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_02_960x1296-3.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x1650_07.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_960x1296_02.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_960x1296_01.jpg"
  - "/uploads/Les_Bons_Faiseurs_Leader_Price_10_960x1296-1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_01_2500x1650-3.jpg"
- template: ligne-8img-4txt
  col8_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_03_2500x1650-1.jpg"
  col4_txt: "#### Une Custom Font spécialement dessinée pour donner à une identité
    aux prises de parole de l'enseigne. Etre simple, être facile, être rond, être
    sympathique... ça ressemble à ça être un soft-discounter."
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x825_21.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_2500x1650_21.jpg"
- template: ligne-4img-4img-4txt
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_960x1296_22.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_960x1296_23.jpg"
  col4_txt: Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur
    at lectus vel hendrerit. Vivamus dictum ullamcorper lacus, et rhoncus odio tristique
    in. Aenean vel urna pretium, hendrerit arcu ac, commodo diam. Proin non nibh posuere,
    mattis sapien eget, lobortis odio. Phasellus id accumsan tortor, nec dictum massa.
weight: 2

---
### Leader Price avait perdu ses racines de discounter. Il était temps de revenir aux préceptes fondateurs qui firent le succès de l'enseigne : faire simple et direct. Le supermarché monomarque à prix discount retrouve enfin l'idée forte du logotype d'origine une flèche bleu vers le haut pour la qualité, une flèche rouge vers le bas pour les prix les plus bas.

Avec l'agence d'architecture Versions, Les Bons Faiseurs installe les bases du nouveau positionnement du soft-discounter français.