---
title: Hang tag
date: 2018-03-08 00:00:00 +0000
client: clients/aigle.md
metiers:
- Branding
- Packaging
image1: "/uploads/aigle1.jpg"
image2: "/uploads/aigle2.jpg"
slider: false
imageslider: "/uploads/aigle1.jpg"
image3: ''
texte_secondaire: ''
prix: true
vignette: "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_833x550-2.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_13_2500x1650.gif"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_02_2500x1650-1.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_14_960x1296-1.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_12_2500.gif"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_06_2500x1650.jpg"
  - "/uploads/AIGLE-4-BD.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_05_2500x1650.jpg"
- template: ligne- 6img-6txt
  col6_txt: "### Comment parler un peu technique et bénéfices tout en revisitant de
    manière très contemporaine l’identité «vintage» de la marque ?"
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Hang_Tag_01_2500x1650.jpg"
recompenses: Prix Club des AD 2014
titre_slider: ''
weight: 2

---
### En 2013, Gideon Day, le directeur artistique de la maison Aigle, fait évoluer la marque de l’outdoor technique vers un univers plus mode, tout en réaffirmant son capital historique et patrimonial. À l'occasion, une nouvelle signature «maître caoutchoutier depuis 1853» est installée.

### Il demande aux Bons Faiseurs de travailler sur les étiquettes présentant les tissus techniques maison.