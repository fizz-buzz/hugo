---
title: 50ans de premières fois
client: clients/semaine-de-la-critique.md
date: 2018-11-16 14:11:28 +0000
metiers:
- Brand content
- Design global
- Communication
prix: false
recompenses: ''
slider: false
titre_slider: 50 ans de première fois.
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Semaine_dela_critique_50ans_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/SIC_09.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/SIC_10.jpg"
  - "/uploads/SIC_04.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SIC_08.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/SIC_07.jpg"
  - "/uploads/SIC_02.jpg"
weight: 10

---
## Fêter les 50 ans de la Semaine de la Critique, voici un joli sujet. Et si en plus de faire la fête on en profitait pour célébrer 50 ans de premières fois.

## En effet, Bernardo Bertolucci, Ken Loach, Wong Kar-wai, Miranda July, Jacques Audiard, Rebecca Zlotowski, Alejandro González Iñárritu, Arnaud Desplechin... ont en commun d'avoir présenter leur premier film et de rencontrer leur premier public à la Semaine de la Critique.

## Beau jubilé.