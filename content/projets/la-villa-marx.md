---
title: La Villa Marx
client: clients/lagardère-travel-retail.md
date: 2018-11-15 16:48:29 +0000
metiers:
- Branding
- Retail
- Design global
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Villa_Marx_6_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_11_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_02_2500x1650-1.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_03_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_VILLAMARX_002_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Villa_Marx_05_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_VILLA_serviette_960x1296.jpg"
- template: ligne-4img-8img
  col4_img: "/uploads/Les_Bons_Faiseurs_Villa_Marx_Menu_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Villa_Marx_132_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_09_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_08_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Villa_Marx_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_VILLAMARX_13_2500x1650.jpg"
weight: 1

---
### C'est dans le pavillon 17 du fabuleux ensemble architectural de l’hôpital Édouard Herriot construit dans les années 30 par l'architecte Tony Garnier que Thierry Marx installe sa première brasserie Lyonnaise.

### Piloté par le groupe Lagardère à qui l'on a confié la revalorisation complète de l’établissement, ce restaurant participe à faire de l’hôpital un lieu de vie ouvert sur la ville.

### Les Bons Faiseurs ont créé une identité délicatement raffinée en rendant hommage à l'architecture de Tony Garnier.