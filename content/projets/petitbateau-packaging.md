---
title: Une marque patrimoniale
date: 2018-09-10 13:54:47 +0000
metiers:
- Packaging
- Brand content
client: clients/petitbateau.md
image1: "/uploads/petitbateau1.jpg"
image2: "/uploads/petitbateau2.jpg"
slider: false
imageslider: "/uploads/petitbateau2.jpg"
texte_secondaire: ''
image3: ''
prix: false
recompenses: ''
vignette: "/uploads/petitbateau1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/petitbateau2.jpg"
titre_slider: ''
weight: 10

---
# Plus que centenaire, Petit Bateau fait partie de la vie intime des français. On se transmet de générations en générations le culte de la culotte Petit Bateau.