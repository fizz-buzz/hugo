---
title: Banette & Moi
client: clients/banette.md
date: 2018-12-07 09:21:30 +0000
metiers:
- Design global
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: "Repositionnement  \nde marque"
imageslider: "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_18.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_833x550_4.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_3.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_5.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_4.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_2.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_19.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_7.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_2.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_7.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_6.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_960x1296_6.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_11.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_16.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_13.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_9.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_17.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_15.jpg"
  - "/uploads/Les_Bons_Faiseurs_BANETTE_ET_MOI_2500x1650_12.jpg"
weight: 1
draft: true

---
Banette est une belle histoire. Cest une aventure au vrai sens du terme : une association d’indépendants qui partagent des outils communs, des expériences et des bonnes pratiques. Une filière de femmes et d’hommes qui ont trouvés les moyens de s’unir face à la grande distribution et l’industrie sans perdre leurs valeurs et leurs indépendances.

Pourtant si l’histoire est belle, elle n’était pas bien racontée. Il était temps de mettre en harmonie l’image et la démarche.

Après 3 ans de travail sur la plate forme de marque, l’identité visuelle, la communication, le concept retail, Les Bons Faiseurs met en mouvement l’aventure autour du positionnement de Banette & Moi.