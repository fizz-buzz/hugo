---
title: Maison Père
client: clients/maison-père.md
date: 2018-10-19 07:56:22 +0000
metiers:
- Branding
- Digital
prix: false
slider: false
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Maison_Pere_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Pere_02_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Pere_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Maison_Pere_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Pere_03_2500x1650.jpg"
recompenses: Nomination Club des AD 201?
titre_slider: ''
weight: 10

---
Fondée en 2014 par Camille Omerin, Maison Père est une marque de mode dont le vestiaire est empreint d’héritage.

Lorsque nous rencontrons Camille, le projet est à son balbutiement, Les Bons Faiseurs lui propose donc de lui dessiner une identité statutaire, chic et intemporelle, pour aider cette toute jeune marque à se developper.