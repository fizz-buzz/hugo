---
title: Projet design global
date: 2018-09-28 12:10:09 +0000
slider: false
metiers:
- Retail
- Brand content
- Design global
client: clients/nicolas.md
image1: "/uploads/nicolas_petites_recoltes_01.jpg"
image2: "/uploads/nicolas_petites_recoltes_02.jpg"
image3: "/uploads/nicolas_petites_recoltes_03.jpg"
imageslider: "/uploads/nicolas_petites_recoltes_03.jpg"
texte_secondaire: ''
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/nicolas_petites_recoltes_04.jpg"
- template: ligne-12txt
  col12_txt: "### Exsistit autem hoc loco quana homine dExsistit autem hoc loco quaedam
    quaestio subdifficilis, num quando amici nubdifficilis, num quando amici novi,
    digni amicitia, veteribus sint anteponendi, ut equis vetulis teneros anteponere
    solemus. Indigna homine dExsistit autem hoc loco quaedam quaestio subdifficilis,
    num quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis
    teneros anteponere solemus. Indigna homind dicitur, multos modios salis simul
    edendos esse, ut amicitiae munus expletum sit."
- template: ligne-6img-6img
  col6_img:
  - "/uploads/nicolas_petites_recoltes_03.jpg"
  - "/uploads/nicolas_petites_recoltes_04.jpg"
- template: ligne-6txt-6txt
  col6_txt_a: |-
    ### Exsistit autem hoc loco

     quaedam quaestio subdifficilis, num quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis teneros anteponere solemus. Indigna homin
  col6_txt_b: sint anteponendi, ut equis vetulis teneros anteponere solemus. Indigna
    homind dicitur, mult
- template: ligne-8img-4img
  col8_img: "/uploads/nicolas_petites_recoltes_04.jpg"
  col4_img: "/uploads/visuel_portrait.jpg"
- template: ligne-8img-4txt
  col8_img:
  - "/uploads/nicolas_petites_recoltes_03.jpg"
  col4_txt: "**Exsistit autem hoddc loco** \n\nquaedam quaestio subdifficilis, num
    quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis
    teneros anteponere solemus. Indigna homine dubitatio! _Non enim debent esse amicitiarum
    sicut aliarum rerum satietates; veterrima_ quaeque, ut ea vina, quae vetustatem
    ferunt, esse debet suavissima; verumque illud est, quod "
  col3_txt: stit autem hoc loco quaedam quaestio subdifficilis, num quando amici novi,
    digni amiciti
- template: ligne-4txt-8img
  col8_img:
  - "/uploads/nicolas_petites_recoltes_03.jpg"
  col4_txt: "**Exsistit autem hoddc loco** \n\nquaedam quaestio subdifficilis, num
    quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis
    teneros anteponere solemus. Indigna homine dubitatio! _Non enim debent esse amicitiarum
    sicut aliarum rerum satietates; veterrima_ quaeque, ut ea vina, quae vetustatem
    ferunt, esse debet suavissima; verumque illud est, quod "
- template: ligne-8txt-4img
  col8_txt: Exsistit autem hoc loco quaedam quaestio subdifficilis, num quando amici
    novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis teneros anteponere
    solemus. Indigna homine dubitatio! Non enim debent esse amicitiarum sicut aliarum
    rerum satietates; veterrima quaeque, ut ea vina, quae vetustatem ferunt, esse
    debet suavissima; verumque illud est, quod dicitur, multos modios salis simul
    edendos esse, ut amicitiae munus expletum sit.
  col4_img:
  - "/uploads/nicolas_petites_recoltes_01.jpg"
- template: ligne-4img-8img
  col4_img: "/uploads/visuel_portrait.jpg"
  col8_img: "/uploads/nicolas_petites_recoltes_01.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/nicolas_petites_recoltes_03.jpg"
  - "/uploads/visuel_paysage.jpg"
  - "/uploads/nicolas_petites_recoltes_04.jpg"
- template: ligne-4txt-4txt-4txt
  col4_txt_a: na homine dExsistit autem hoc loco quaedam quaestio subdifficilis, num
    quando amici n
  col4_txt_b: tem hoc loco quaedam quaestio subdifficilis, num quando amici novi,
    digni amicitia, vete
  col4_txt_c: " **ut equis vetulis teneros anteponere solemus. Indigna homine dExsistit
    autem hoc loco quaedam quaestio subdifficilis, num quando amici novi, digni amicitia,
    veteribus sint anteponendi, ut equis vetulis teneros anteponere solemus. Indigna
    homind dicitur, multo**"
vignette: "/uploads/visuel_paysage.jpg"
prix: true
recompenses: ''
titre_slider: ''
weight: 10
draft: true

---
Exsistit autem hoc loco quana homine dExsistit autem hoc loco quaedam quaestio subdifficilis, num quando amici nubdifficilis, num quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis teneros anteponere solemus. Indigna homine dExsistit autem hoc loco quaedam quaestio subdifficilis, num quando amici novi, digni amicitia, veteribus sint anteponendi, ut equis vetulis teneros anteponere solemus. Indigna homind dicitur, multos modios salis simul edendos esse, ut amicitiae munus expletum sit.