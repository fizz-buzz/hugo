---
title: Célio
client: clients/celio.md
date: 2018-11-15 16:41:14 +0000
metiers:
- Retail
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Celio_Nouvelle_Identite_833x550.jpg"
ligne_col:
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Celio_Nouvelle_Identite_31_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Celio_Nouvelle_Identite_19_2500x1650.jpg"
weight: 10

---
