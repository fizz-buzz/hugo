---
title: Warm Socks
client: clients/aigle.md
date: 2018-11-15 09:27:12 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_06_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_04_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_02_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Aigle_Warm_Socks_01_2500x1650.jpg"
weight: 3

---
