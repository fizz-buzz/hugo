---
title: Lafayette Gourmet
client: clients/galeries-lafayette.md
date: 2018-11-16 14:37:08 +0000
metiers:
- Packaging
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_10_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_04_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_05_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lafayette_Gourmet_08_2500x1650.jpg"
weight: 100

---
L'art et la manière d'être un fin gourmet expliqué en 6 animations in-store. Bravo à Caroline pour ces belles peintures.