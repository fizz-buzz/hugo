---
title: Des hommes et des femmes au service du plus beau des métiers.
client: clients/banette.md
date: 2018-11-26 12:32:23 +0000
metiers:
- Communication
- Brand content
prix: false
recompenses: ''
slider: true
titre_slider: "Campagne publicitaire  \n2018-2019"
imageslider: "/uploads/Les_Bons_Faiseurs_BANETTE_CAMPAGNE_2018_833x550-1.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_BANETTE_CAMPAGNE_2018_833x550-1.jpg"
ligne_col:
- template: ligne12video
  col12_video: https://www.youtube.com/embed/9CjpUVnZ0Fk
  video_controls: true
- template: ligne-12txt
  col12_txt: "##### 10 films publicitaires - _qui ne ressemblent pas à de la publicité_
    - avec un plan média ambitieux. Diffusion BFMTV tous les jours en 2018 et 2019.
    Ainsi que sur les chaînes Youtube et les réseaux sociaux des artisans."
- template: ligne12video
  col12_video: https://www.youtube.com/embed/TG_m09CoLVs
  video_controls: true
- template: ligne12video
  col12_video: https://www.youtube.com/embed/WRfPHTp78v8
  video_controls: true
- template: ligne12video
  col12_video: https://www.youtube.com/embed/xjgjEMvv2hQ
  video_controls: true
- template: ligne12video
  video_controls: true
  col12_video: https://www.youtube.com/embed/45aXZRCgK7s
- template: ligne12video
  video_controls: true
  col12_video: https://www.youtube.com/embed/xX1IrFewyd8
- template: ligne12video
  video_controls: true
  col12_video: https://www.youtube.com/embed/U68R-H4g4TU
- template: ligne12video
  col12_video: https://www.youtube.com/embed/ZuRuVztS9TU
  video_controls: true
- template: ligne12video
  col12_video: https://www.youtube.com/embed/K9DSZ_nZNsM
  video_controls: true
- template: ligne12video
  col12_video: https://www.youtube.com/embed/WsCPBuvgRVA
  video_controls: true
weight: 1

---
## Laissons parler ceux qui font, ils parlerons mieux que n'importe qui. 

## Banette désirait repartir en campagne télé. Tournant volontairement le dos aux écritures publicitaires, Les Bons Faiseurs décident de faire parler les coeurs, les vrais, ceux qui font de la boulangerie le plus beau des métiers.

## Ils ont demandés à Patricia et Éric Adelheim de traverser la France à la rencontre des hommes et des femmes du mouvement Banette. Ils sont revenus avec des heures et des heures de matières, qui sont devenus dix portraits de gens qui racontent la véritable et belle histoire qu'est Banette. 

## Et comme le dit si bien Nicolas, _"tu ne peux pas tricher, tu fais simple, tu fais bien et tu fais bon"_.