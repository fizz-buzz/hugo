---
title: Les objets Banette
client: clients/banette.md
date: 2018-11-15 15:54:32 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Objets_833x550.jpg"
ligne_col:
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_01_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_04_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_05_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Objets_06_2500x1650.jpg"
weight: 10

---
