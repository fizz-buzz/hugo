---
title: Lapain
client: clients/banette.md
date: 2018-11-15 14:29:16 +0000
metiers:
- Packaging
- Brand content
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Lapain_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_02_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Lapain_04_2500x1650.jpg"
weight: 10

---
