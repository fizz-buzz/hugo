---
title: Truffés de Saveurs
client: clients/banette.md
date: 2018-11-15 15:17:45 +0000
metiers: []
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Truffes_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Truffes_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Truffes_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Truffes_01_2500x1650.jpg"
weight: 10

---
