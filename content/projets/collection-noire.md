---
title: Collection Noire
client: clients/celio.md
date: 2018-11-15 16:32:47 +0000
metiers:
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Celio_Collection_Noire_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Celio_Collection_Noire_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Celio_Collection_Noire_04_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Celio_Collection_Noire_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Celio_Collection_Noire_06_2500x1650.jpg"
weight: 10

---
