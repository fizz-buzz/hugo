---
title: Légion d'honneur
date: 2018-09-24 22:00:00 +0000
client: clients/grande-chancellerie-de-la-légion-d-honneur.md
metiers:
- Branding
slider: false
image1: "/uploads/legionhonneur1.jpg"
image2: "/uploads/legionhonneur2.jpg"
imageslider: "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_04_2500x1650.jpg"
image3: ''
texte_secondaire: |-
  Pourtant la Grande Chancellerie demeure méconnue des français.

  En 2013, elle entame un chantier de reconquête de son image auprès des institutions et du grand public et confie aux bons faiseurs la mission de créer une identité visuelle porteuse de ses valeurs et de son histoire.
vignette: "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_833x550-1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_04_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_07_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_13_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_12_960x1296.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_02_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_03_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_01_2500x1650-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_05_2500x1650.jpg"
- template: ligne-4img-4img-4txt
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_07_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Grande_Chancellerie_01_2500x1650-1.jpg"
  col4_txt: "Pourtant la Grande Chancellerie demeure méconnue des français.  \n\nEn
    2013, elle entame un chantier de reconquête de son image auprès des institutions
    et du grand public et confie aux bons faiseurs la mission de créer une identité
    visuelle porteuse de ses valeurs et de son histoire.  "
- template: ligne-12txt
  col12_txt: |-
    Un entrelacs de feuilles de chêne et de laurier.

    Depuis l'antiquité, il est coutume de remettre des couronnes de feuilles aux méritants. Le feuillage désignait les mérites. Les rameaux de chêne étaient réservés aux actes civils. Les rameaux de laurier récompensaient les actes de bravoures militaires.

    La Légion d’honneur est destiné à récompenser tout autant le courage des militaires et le talent des civils, placant ainsi à égalité les méritants de la république.

    Les Bons Faiseurs dessinent un entrelacs de feuilles de chêne et de laurier pour la Grande chancellerie.
prix: false
recompenses: ''
titre_slider: "Grande Chancellerie  \nde la Légion d'Honneur"
weight: 3

---
### Créée par Napoléon en 1802, la Légion d’honneur est la plus haute décoration française. Elle récompense les mérites acquis par les citoyens, en dehors de toute considération sociale ou héréditaire et ce, dans tous les secteurs d’activité du pays, civils ou militaires. En 200 ans d’existence, cette institution est devenue un marqueur fort et toujours vivace de la société française.

### Pourtant la Grande Chancellerie demeure méconnue des français. Elle entame un chantier de reconquête de son image auprès des institutions et du grand public et confie aux Bons Faiseurs la mission de créer une identité visuelle porteuse de ses valeurs et de son histoire.