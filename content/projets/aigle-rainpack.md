---
title: Gamme Rainpack
client: clients/aigle.md
date: 2018-11-15 09:08:02 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: Création de marque
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Aigle_Rainpack_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Rainpack_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Rainpack_02_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_RAINPACK_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_RAINPACK_960x1296_5.jpg"
  - "/uploads/Les_Bons_Faiseurs_RAINPACK_960x1296_4.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Aigle_Rainpack_03_2500x1650.jpg"
weight: 2

---
## _C'est la supriseparty à la grenouille !_

## La célèbre marque française spécialisée dans le caoutchouc ne finit pas d'innover en créant Rainpack® : une gamme de vêtements et de bottes pliables à dégainer en cas de tempête !

## Les Bons Faiseurs dessinent un logo-image au charme vintage et des packagings malins.