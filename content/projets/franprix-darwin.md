---
title: Franprix Darwin
client: clients/franprix.md
date: 2019-01-15 16:31:06 +0100
metiers:
- Design global
- Brand content
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_FRANPRIX_DARWIN_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_FRANPRIX_DARWIN_2500x1650_1.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_FRANPRIX_DARWIN_960x1296_1.jpg"
  - "/uploads/Les_Bons_Faiseurs_FRANPRIX_DARWIN_960x1296_2.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_FRANPRIX_DARWIN_2500x1650_2.jpg"
weight: 3
draft: true

---
