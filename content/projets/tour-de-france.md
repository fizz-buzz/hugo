---
title: Tour de France
client: clients/banette.md
date: 2018-11-15 15:06:35 +0000
metiers:
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_TDF_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_04_2500x1650-1.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_06_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_08_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_TDF_01_2500x1650.jpg"
weight: 10

---
