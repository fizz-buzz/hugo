---
title: Les Petites Récoltes Édition spéciale
date: 2018-09-28 12:10:09 +0000
slider: false
metiers:
- Packaging
client: clients/nicolas.md
image1: "/uploads/nicolas_petites_recoltes_01.jpg"
image2: "/uploads/nicolas_petites_recoltes_02.jpg"
image3: "/uploads/nicolas_petites_recoltes_03.jpg"
imageslider: "/uploads/Les_Bons_Faiseurs_Nicolas_Les_Petites_Recoltes_2014_05_2500x1650.jpg"
texte_secondaire: ''
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/nicolas_petites_recoltes_04.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/nicolas_petites_recoltes_03.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/nicolas_petites_recoltes_02.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/nicolas_petites_recoltes_01.jpg"
- template: ligne-8img-4txt
  col3_txt: TESTESTES
  col8_img:
  - "/uploads/Petites_Recoltes_HOME.jpg"
  col4_txt: "Ce que disait l'étude :\n\n_Le monde du vin a bien changé en 25 ans._\n\n_L'impertinence
    d'hier n'a plus de résonance aujourd'hui._ \n\n_Les jeunes consommateurs souhaitent
    acquérir la culture du vin tout en respectant ses codes._"
- template: ligne-4img-4img-4txt
  col4_img:
  - "/uploads/Petites_Recoltes_HOME.jpg"
  - "/uploads/nicolas_petites_recoltes_01.jpg"
  col4_txt: |-
    Notre analyse :

    _Corriger l’équilibre entre impertinence et code traditionnel. Sans perdre nos racines donner aux petites récoltes des nouveaux codes tendances pour apporter respectabilité._
vignette: "/uploads/Les_Bons_Faiseurs_Nicolas_Les_Petites_Recoltes_2014_833x550.jpg"
prix: false
recompenses: ''
titre_slider: "Les Petites Récoltes  \nÉdition spéciale"
weight: 11

---
Cette série spéciale (éphémère) était l’occasion idéale de raviver les Petites Récoltes. Créées il y a plus de 25 ans par le talentueux Olivier Saguez, les Petites Récoltes avaient vécu. Sur la base d'une étude consommateurs et d'un travail préparatoire des équipes marketing de Nicolas et d'Isabelle Johannet, Les Bons Faiseurs ont dessiné une version tout en maturité de l’ancien enfant terrible des vins de pays.