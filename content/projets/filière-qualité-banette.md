---
title: Filière Qualité Banette
client: clients/banette.md
date: 2018-11-15 14:12:10 +0000
metiers: []
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_FQB_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_06_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_04_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_FQB_08_2500x1650.jpg"
weight: 10

---
