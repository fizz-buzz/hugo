---
title: Typographie identitaire pour Leader Price
client: clients/leader-price-1.md
date: 2018-11-20 15:33:24 +0000
metiers:
- Design global
prix: false
recompenses: ''
slider: false
titre_slider: Brand font
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Leader_Price_02_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LEADERPRICE_04_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_LEADERPRICE_02_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_04_960x1296.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Leader_Price_10_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_03_960x1296-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_LEADER_PRICE_02_960x1296.jpg"
weight: 1
draft: true

---
