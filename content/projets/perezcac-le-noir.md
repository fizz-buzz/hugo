---
title: Saucisson de Village
client: clients/jean-stalaven.md
date: 2018-12-12 10:08:42 +0000
metiers:
- Packaging
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: Création de marque
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_STALAVEN3_833x550-1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/STALAVEN_05_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/STALAVEN_2500x1650_3.jpg"
- template: ligne-12txt
  col12_txt: "#### Le premier Saucisson de Village de la série vient de Perpezac-le-Noir.
    Village de Corrèze où depuis plus de 60 ans les Salaisons Boutot produisent un
    saucisson affiné pendant 6 longues semaines et qui développe une saveur inimitable."
- template: ligne-6img-6img
  col6_img:
  - "/uploads/STALAVEN_09_2500x1650-1.jpg"
  - "/uploads/STALAVEN_08_2500x1650-1.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/STALAVEN_02_2500x1650.jpg"
  col4_img: "/uploads/STALAVEN_02_2960x1296.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/STALAVEN_06_2960x1296.jpg"
  - "/uploads/STALAVEN_08_2960x1296.jpg"
  - "/uploads/STALAVEN_07_2960x1296.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/STALAVEN_06_2500x1650.jpg"
  - "/uploads/STALAVEN_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/STALAVEN_2500x1650_2.jpg"
weight: 2

---
## Jean Stalaven veut conquérir le marché de la salaison sèche chez les charcutiers traiteurs.

## Pour cela, Les Bons Faiseurs recommandent de valoriser le charcutier dans son contrat relationnel qui le lie à ses clients en positionnant le saucisson non pas comme un produit de revente mais comme le fruit d'un travail de sélection de produits de terroir.

## Les Saucissons de Village : une sélection de saucissons d’origine dénichés pour vous, par votre charcutier.