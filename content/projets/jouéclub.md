---
title: Nouvelle identité JouéClub
client: clients/jouéclub.md
date: 2018-11-16 10:52:55 +0000
metiers:
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: Identité visuelle
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_JoueClub_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_10_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_01_2500x1650-1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_17_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_16_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_15_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_12_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_JoueClub_11_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_JoueClub_11_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_JoueClub_09_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_JoueClub_03_960x1296-2.jpg"
weight: 1

---
# Dans un marché très éclaté et chahuté, Jouéclub se devait de s’affirmer face à son marché. L’entreprise coopérative entreprend de nombreux chantiers dont l’épineux dossier de son identité  visuelle. C’est aux Bons Faiseurs que revient la charge de débarrasser Jouéclub de ses oripeaux et doter l’entreprise d’une identité à l’image de son dynamisme et sa modernité.