---
title: Le Diamant Doré
client: clients/comtesse-du-barry.md
date: 2018-11-16 09:06:58 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Diamant_Dore_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Diamant_Dore_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Diamant_Dore_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Diamant_Dore_03_2500x1650.jpg"
weight: 20

---
