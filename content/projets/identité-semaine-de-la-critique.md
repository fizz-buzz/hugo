---
title: Identité Semaine de la Critique
client: clients/semaine-de-la-critique.md
date: 2018-11-16 14:30:52 +0000
metiers:
- Design global
- Communication
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Semaine_dela_critique_833x550_2.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine_Dela_Critique_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SIC_14.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SIC_12.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/SIC_22.jpg"
  - "/uploads/SIC_20.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/SIC_18.jpg"
  - "/uploads/SIC_17.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SIC_24.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine_dela_critique_02_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/SIC_19-2.jpg"
  - "/uploads/SIC_15.jpg"
weight: 10

---
