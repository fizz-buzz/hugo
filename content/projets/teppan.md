---
title: Teppan
client: clients/lagardère-travel-retail.md
date: 2018-11-11 11:51:54 +0000
metiers:
- Retail
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Teppan_2_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Teppan_logo_2500x1650.jpg"
- template: ligne-4img-8img
  col8_img: "/uploads/Les_Bons_Faiseurs_Teppan_FACADE3_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Teppan_GRILL_960x1296.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_Teppan_SacVertical3_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Teppan_cafe_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Teppan-T.Marx_66_03-2018.jpg"
  - "/uploads/Teppan-T.Marx_53_03-2018.jpg"
  - "/uploads/Teppan-T.Marx_28_03-2018.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_TEPPAN_menutest2_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_Teppan_tablier2_2500x1650-1.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Teppan_tablier3_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Teppan_MOUSSELINE3_2500x1650.jpg"
- template: ligne-4img-8img
  col4_img: "/uploads/Les_Bons_Faiseurs_Teppan_serviette_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_TEPPAN_setdetable-1.jpg"
weight: 1

---
### Teppan est le nouveau restaurant de Thierry Marx au Terminal 1 de l’aéroport Paris-Charles de Gaulle. Chef doublement étoilé, à la tête de plusieurs restaurants en France et au Japon, Thierry Marx puise son inspiration de ses nombreux voyages.

### Le restaurant tire son nom du teppanyaki, littéralement « grillé sur une plaque de fer » en japonais.  Le teppanyaki est un véritable outil de précision permettant une maîtrise parfaite des températures et une cuisine rapide qui exalte les saveurs des produits proposés.

### Les Bons Faiseurs accompagnent l’agence d’architecture Versions dans le projet et créent une identité du lieu au croisement de la culture parisienne et internationale.