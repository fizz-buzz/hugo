---
title: Mandarine
client: clients/franprix.md
date: 2018-11-16 09:28:14 +0000
metiers:
- Retail
- Branding
- Design global
prix: true
recompenses: Janus du commerce 2016 - Top Com d'Or Design Global 2016
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Franprix_02_833x550.jpg"
ligne_col:
- template: ligne12video
  col12_video: https://player.vimeo.com/video/157720773
  video_controls: false
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/150704clery079sw.jpg"
  - "/uploads/CDB_IDENTITE_FACADE.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_24_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Franprix_23_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_09_960x1296-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_Franprix_10_960x1296-2.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_14_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Franprix_12_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Franprix_07_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Franprix_13_960x1296.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Franprix_15_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Franprix_16_960x1296.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_17_2500x1650-1.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_21_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Franprix_19_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Franprix_20_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Franprix_22_2500x1650.jpg"
weight: 2

---
Mandarine est le joli nom du concept qui a révolutionné Franprix. Avec une prétention, _pas si mal assumée_, on dira qu'il y a un avant et un après Mandarine. Un avant et un après pour l'équipe des Bons Faiseurs ainsi que pour les équipes de Franprix. Un avant et un après pour les urbains. Pour le commerce, pour la proximité. Pour l'expérience et la vie en ville. Pour la dette et le contrat que l'on doit envers toute personne qui a fait l'effort de pousser la porte d'un commerce...