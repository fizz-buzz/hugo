---
title: Asia
client: clients/jour.md
date: 2018-11-16 13:54:51 +0000
metiers:
- Packaging
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Jour_Asia_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Jour_Asia_01_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_03_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_09_960x1296.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_04_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_10_960x1296.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_12_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Jour_Asia_11_960x1296.jpg"
weight: 40

---
