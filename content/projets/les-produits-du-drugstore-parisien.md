---
title: 300 packagings à la marque Drugstore
client: clients/le-drugstore-parisien.md
date: 2018-11-27 16:52:17 +0000
metiers:
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: M.D.D.
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Client_LE_DRUGSTORE_MDD_01_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_01_2500x1406.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_02_ACCESSOIRES_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_04_CREMES_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_03_KITS-DE-SURVIE_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_06_CARTES-POSTALES_2500x1650.jpg"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_05_CARTES-ANNIVERSAIRE_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_07_BOULES-DE-NOEL_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_BOULE-ROSE_2500x1650.jpg"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_BOULE-BLEUE_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_BOULE-VIOLETTE_2500x1650.jpg"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_BOULE-ROUGE_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BOULES_MERE-POULE_2500x1650.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_08_ECUSSONS_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-MDD-ECUSSONS-3_2500x1650.gif"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-MDD-ECUSSONS-2_2500x1650.gif"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-MDD-ECUSSONS-1_2500x1650.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MOUCHOIRS_2500x1650.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_09_MOUCHOIRS_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_TOTE-BAGS_2500x1650.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_25.gif"
- template: ligne-4img-8img
  col4_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_10-1.gif"
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_17_2.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_20.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_5.gif"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_3.gif"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_4.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_29.gif"
- template: ligne-4img-8img
  col4_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_11.gif"
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_08_2500x1650-1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_26.gif"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_19.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_15-1.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_28.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_21-3.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_18-1.jpg"
- template: ligne-12txt
  col12_txt: 'Résultat : la marque la plus vendue du drugstore.'
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_8-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_12-1.gif"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_14.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_16_2.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_960x1296_6.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_MDD_2500x1650_15-2.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-VAILLANT_2500x1650-1.jpg"
  col4_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_GEL-VAILLANT_960x1296.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-HARDI_2500x1650.jpg"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-TEMERAIRE_2500x1650.jpg"
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-ARDENT_2500x1650.jpg"
- template: ligne-12txt
  col12_txt: |-
    ##### Un lancement de plus de 200 références en 4 mois grâce à une collaboration ultra-fluide avec les équipes du drugstore.

    ###### _Un spécial merci/bravo à Delphine Bresson et Sabrina Petraloni._
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/39699091_2153012348305636_6094787119643885568_n-1.jpg"
  - "/uploads/38709797_518317698596750_4021921596334669824_n-1.jpg"
  - "/uploads/41792244_290310928360290_7578344766371201862_n-1.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/46679559_338546970330965_6274636600123853370_n.jpg"
  - "/uploads/47483508_1789950784443126_8222358970684919498_n.jpg"
  - "/uploads/44663740_224217555177867_8827813356465302406_n.jpg"
weight: 2

---
## _"Objets inanimés, avez-vous donc une âme qui s’attache à notre âme et la force d’aimer ?"_ se questionnait Alphonse de Lamartine. Chez les LBF et au Drugstore Parisien on pense que les objets ont de l'esprit, du malicieux, de l'urban-culture, du frivole, du joli et surtout un "_je ne sais quoi de parisien"_...