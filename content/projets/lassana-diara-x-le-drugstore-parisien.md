---
client: clients/le-drugstore-parisien.md
title: Lassana Diara x ...le drugstore parisien
date: 2019-04-10 00:00:00 +0200
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_VIGNETTE_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-VAILLANT_2500x1650-2.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_GEL-VAILLANT_960x1296-1.jpg"
  col8_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_INSTAGRAM-VAILLANT_1500x990.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-ARDENT_2500x1650-1.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_GEL-ARDENT_960x1296.jpg"
  col8_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_INSTAGRAM-ARDENT_1500x990.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-HARDI_2500x1650-1.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_GEL-HARDI_960x1296.jpg"
  col8_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_INSTAGRAM-HARDI_1500x990.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_COMPOSITION-TEMERAIRE_2500x1650-1.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_INSTAGRAM-TEMERAIRE_1500x990.jpg"
  col4_img: "/uploads/LES-BONS-FAISEURS_LE-DRUGSTORE-PARISIEN_MDD_BY-LASS_GEL-TEMERAIRE_960x1296.jpg"
weight: ''

---
