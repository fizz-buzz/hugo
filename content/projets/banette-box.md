---
title: Banette Box
client: clients/banette.md
date: 2018-11-15 15:27:10 +0000
metiers: []
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Box_833x550.jpg"
ligne_col:
- template: ligne-12txt
  col12_txt: "# Banette Box 2014"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_04_2500x1650-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_03_2500x1650.jpg"
- template: ligne-12txt
  col12_txt: "# Banette Box 2013"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_06_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_07_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Banette_Box_05_2500x1650.jpg"
weight: 10

---
