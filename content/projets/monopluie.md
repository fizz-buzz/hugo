---
title: Monop'luie
client: clients/monoprix.md
date: 2018-11-20 11:42:06 +0100
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Monopluie_04_2500x1650.jpg"
weight: ''

---
