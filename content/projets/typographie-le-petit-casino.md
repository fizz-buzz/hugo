---
title: Création d’une police de caractère
client: clients/petit-casino.md
date: 2018-11-16 16:28:42 +0000
metiers:
- Branding
- Design global
prix: true
recompenses: TYPOGRAPHIE CLUB DES DA 2017
slider: false
titre_slider: "Typographie  \nLe Petit Casino"
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_PETIT_CASINO_2500x1650_2.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_04_2500x1650.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_PetitCasino_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_08_06_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_23_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_24_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_09_33x550.jpg"
weight: 1

---
## Le nouveau concept des Petits Casino est d’incarner non pas la proximité mais les proximités. Derrière chaque magasin, il y a une histoire, une équipe, un quartier, une ville, un village. Pour illustrer cet ancrage nous avons créé autant d’enseignes que de magasins en associant Le Petit Casino avec le nom de chaque ville, village ou quartier.

## L’idée est bonne, mais comment faire quand le nom est à rallonge ?

## Une police de caractère qui s’adapte et se compacte fut dessinée par Thomas et Roland avec l’aide de la main de maître de Christophe Badani.