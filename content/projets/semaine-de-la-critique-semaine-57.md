---
client: clients/semaine-de-la-critique.md
title: Semaine de la Critique - Semaine 57
date: 2019-03-11 00:00:00 +0100
metiers:
- Communication
- Brand content
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Semaine_Critique_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_08_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_07_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_05_960x1296.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_02_2500x1650.jpg"
- template: ligne-8img-4txt
  col8_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_04_2500x1650.jpg"
  col4_txt: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at consectetur
    velit, et condimentum felis. Praesent a erat sit amet velit faucibus imperdiet
    eget porta eros. Aenean id placerat lorem. Aenean faucibus nunc a lectus tincidunt
    scelerisque. Curabitur vel eros ut tellus condimentum ullamcorper.
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_10_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_15_960x1296.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_11_2500x800.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_12_2500x800.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_13_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Semaine-Critique_14_2500x800.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Semaine-Critique_16_2500x800.jpg"
weight: ''

---
