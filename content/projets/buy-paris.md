---
title: Buy Paris Duty Free
client: clients/lagardère-travel-retail.md
date: 2018-11-11 11:53:25 +0000
metiers:
- Communication
- Retail
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Buy_Paris_VIGNETTE2.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_04_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Buy_Paris_08_2500x1650.jpg"
weight: ''

---
Paris est une des destinations touristiques préférées dans le monde. C'est un fait. Mais comment tirer parti de _ce je ne sais quoi_ qui fait cet engouement quand on est le leader du duty free en france ?

Les Bons Faiseurs créent 18 visuels mixant les monuments parisiens et les produits emblématiques du french art de vivre.

La petite idée qui change tout : des photos prises de jour puis inversées. Bravo à Adolfo Fiori pour ces photos icônes.