---
title: Villa Ponciago
client: clients/bouchard.md
date: 2018-11-11 12:27:18 +0000
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_VILLA_PONCIAGO_833x550_2.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Ponciago_08_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Villa_Ponciago_01_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Villa_Ponciago_02_2500x1650-1.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_VILLA_PONCIAGO_02_960x1296.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Villa_Ponciago_05_2500x1650-1.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_VILLA_PONCIAGO_01_960x1296.jpg"
weight: 3

---
## En l'an 949, le propriétaire du Château de Poncié a fait don de sa vigne à l'abbaye de Cluny, avec l'espoir de sauver son âme.

## Aujourd'hui, le Château de Poncié retrouve son nom latin d'origine, Villa Ponciago, avec l'ambition de redécouvrir les grands vins du Cru Fleurie ; vins dont la finesse, l'élégance et surtout la texture soyeuse forment déjà leur réputation d'excellence.

Une jolie rencontre avec Thomas Henriot et Stéphane Mellier.