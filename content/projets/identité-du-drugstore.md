---
title: Le bazar de la tendance…
client: clients/le-drugstore-parisien.md
date: 2018-11-23 13:27:28 +0000
metiers:
- Retail
- Design global
- Branding
prix: false
recompenses: ''
slider: true
titre_slider: Le Bazar réinventé…
imageslider: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_10_2500x1650.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_Le_drugstore_parisien__01_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_07_2500x1650.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_Le_Drugstore_Parisien_04_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_08_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_03_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_05_2500x1650.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/43621836_308723743186094_2211930927829522311_n.jpg"
  - "/uploads/36732462_881730682032058_3179289213753360384_n.jpg"
  - "/uploads/34922497_608549002858350_5506263878675202048_n.jpg"
- template: ligne-12txt
  col12_txt: Une collaboration avec l'agence d'architecture Versions.
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/36160729_204103233632819_8687524748705398784_n.jpg"
  - "/uploads/42003971_699771260356310_5668389104340085370_n.jpg"
  - "/uploads/38426517_502000393560894_8626724246770417664_n.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Drugstore_Parisien_960x1296_02.jpg"
  - "/uploads/Les_Bons_Faiseurs_Le_Drugstore_Parisien_960x1296_03.jpg"
  - "/uploads/Les_Bons_Faiseurs_Le_Drugstore_Parisien_960x1296_01.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/42178101_721801948152960_3153446140285005855_n.jpg"
  - "/uploads/40032587_519619168488208_7186011311592446492_n.jpg"
  - "/uploads/37520482_263106994318736_6019858318493745152_n.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SAC_PAPIER_GAMME.gif"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/40605587_1864751506927378_1653820014874174588_n-1.jpg"
  - "/uploads/40251868_244444019592850_5614797865814442542_n-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_DRUGSTORE_PARISIEN_833x550_02.jpg"
weight: 1

---
## Nouveau temple de l'envie, de l'inattendu, de la tendance urbaine ...le drugstore parisien est une invention. Un nouveau concept mélangeant joyeusement l'indispensable, le futile et l'urbain. Il est la version moderne des Bazars Parisiens qu'Émile Zola décrit si bien dans le Bonheur des Dames.