---
title: Maison St Geours
date: 2018-09-28 12:10:09 +0000
slider: false
metiers:
- Branding
client: clients/labeyrie.md
image1: "/uploads/nicolas_petites_recoltes_01.jpg"
image2: "/uploads/nicolas_petites_recoltes_02.jpg"
image3: "/uploads/nicolas_petites_recoltes_03.jpg"
imageslider: ''
texte_secondaire: ''
ligne_col:
- template: ligne12video
  col12_video: https://player.vimeo.com/video/157723468
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_11_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_13_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_12_960x1296.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_02_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_06_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_10_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_15_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_14_2500x1650.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_Maison_Saint_Geours_833x550_4.jpg"
prix: false
recompenses: ''
titre_slider: ''
weight: 3

---
## Labeyrie teste un nouveau concept de corner «traiteur» dans les hypermarchés. Une offre premium est proposée à la vente avec une théâtralisation sur place : saumon fumé sur place et tranché à la demande, pâtisseries préparées devant vous, macarons fourrés à la poche à douille au comptoir...

## Ils confient aux Bons Faiseurs la création du nom du concept, de son identité visuelle et de sa communication sur le point de vente.

## Le nom fait référence à Saint-Geours-de-Maremne, ville où se trouvent les ateliers de fabrication de Labeyrie.