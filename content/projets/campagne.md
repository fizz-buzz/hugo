---
title: Campagne
client: clients/banette.md
date: 2018-11-15 11:42:43 +0000
metiers:
- Brand content
- Communication
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Banette_Campagne_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Banette_Campagne_01_2500x1650.jpg"
- template: ligne12video
  col12_video: https://player.vimeo.com/video/104999240
weight: 10

---
