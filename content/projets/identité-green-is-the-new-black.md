---
title: Green is the new black
client: clients/jour.md
date: 2018-11-16 14:36:13 +0000
metiers:
- Branding
- Brand content
prix: false
recompenses: ''
slider: false
titre_slider: Nouvelle identité visuelle
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Jour_01_833x550-1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Client_Projet_JOUR_GIF_2500x1650_GOOD-1.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_01-1.jpg"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_JOUR_960x1296_1-1.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_02-1.jpg"
- template: ligne-4img-8img
  col4_img: "/uploads/Les_Bons_Faiseurs_Jour_02_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Jour_06_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_10.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_09.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Jour_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_06.jpg"
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_08.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_05.jpg"
  - "/uploads/Les_Bons_Faiseurs_JOUR_2500x1650_04.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SAC_PAPIER_GITNB_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/CARTES_VISITES_2500x1650.jpg"
weight: 1

---
### Sous l'impulsion de son talentueux fondateur Thomas Batistini, les restaurants de salades sur mesure Jour ont demandé aux Bons Faiseurs de redéfinir leur image. La mission : créer la préférence en affirmant les engagements du leader français du marché. Les outils : une nouvelle identité visuelle plus mode et plus impactante mais aussi une communication sur les valeurs de l'enseigne. Valorisant le sourcing des produits, le partenariat avec les fournisseurs, la démarche d'écoresponsabilité, et bien d'autres engagements.

### Le tout habillé par une nouvelle signature : Healthy by nature.