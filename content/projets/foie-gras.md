---
title: Foie Gras
client: clients/comtesse-du-barry.md
date: 2018-11-15 17:30:25 +0000
metiers:
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_03_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_04_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Packaging_Foie_Gras_05_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Foie_Gras_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Foie_Gras_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Foie_Gras_05_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Comtesse_Du_Barry_Animation_Foie_Gras_04_2500x1650.jpg"
weight: 10

---
