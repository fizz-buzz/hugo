---
client: clients/jeff-de-bruges.md
title: Les Écorces
date: 2019-03-30 00:00:00 +0100
metiers:
- Branding
- Packaging
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_VIGNETTE_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_01_INTRODUCTION_2500x1650-1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_13_TABLETTES_2500x1650-2.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_07_ETIQUETTE-VIOLET_2500x1650-1.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_10_EXPLOSION-BLEUE_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_11_EXPLOSION-ROSE_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/LES-BONS-FAISEURS_JEFF-DE-BRUGES_ECORCES_02_COLLECTION_2500x1650.jpg"
weight: ''

---
### _Les Écorces, avant de les critiquer, goûtez-les !_

### La générosité est le meilleur compagnon de la gourmandise. Les maxi-tablettes de Jeff de Bruges ne sont pas généreuses, elles sont très généreuses et donc très très gourmandes.

### Il fallait rajouter juste un peu de truculence pour finir de les emballer.