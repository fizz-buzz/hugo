---
title: Les Lupains
client: clients/moulin-de-bachasson.md
date: 2018-11-16 15:22:56 +0000
metiers:
- Branding
prix: true
recompenses: FOOD AND BREVERAGE Awards catégorie Brand Identity 2014 - Prix Design
  Stratégie Catégorie Identité Visuelle 2011 - Club des DA 2011 - Super Design 2011
slider: false
titre_slider: ''
imageslider: "/uploads/Les_Bons_Faiseurs_Lupains_10_2500x1650-1.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_Lupains_833x550.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_10_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_06_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Lupains_05_2500x1650.jpg"
- template: ligne12video
  col12_video: https://player.vimeo.com/video/69950425
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_04_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_01_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_09_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Lupains_08_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Lupains_03_2500x1650.jpg"
weight: 9

---
### _Le merveilleux recueil aux graines._

#### À la manière d'un herbier et des planches des encyclopédistes, les Bons Faiseurs ont créé un jeu graphique reprenant les céréales utilisées dans les recettes Lupains.

#### Les 14 graines sont les 14 éléments de l’identité. Combinés, ils sont le motif générique de la marque. Isolés, ils sont le motif spécifique d’une recette.

#### Ce jeu graphique imaginé par Fanny s’exprime dans un noir-bleu sur fond blanc, laissant la couleur des pains dynamiser l’ensemble. Simple et universel.