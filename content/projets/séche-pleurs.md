---
title: Séche-pleurs
client: clients/monoprix.md
date: 2018-11-01 19:00:11 +0000
metiers:
- Packaging
prix: true
recompenses: "A Design Award 2017 - Top Com D’or 2010 - European Design Awards 2010
  \ \nSuper Design 2010"
slider: false
imageslider: ''
vignette: "/uploads/MONOPRIX_SECHEPLEURS_HOME.jpg"
ligne_col:
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_12_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_13_2500x1650.gif"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_15_960x1296-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_14_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_16_960x1296.gif"
- template: ligne-8img-4img
  col4_img: "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_17_960x1296.jpg"
  col8_img: "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_18_2500x1650.gif"
- template: ligne12video
  col12_video: https://player.vimeo.com/video/69974105
  video_controls: false
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_02_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_11_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/SECHE_PLEURS_4.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_10_2500x1650-2.jpg"
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_06_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_09_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_Monoprix_Seche_Pleurs_08_2500x1650-1.jpg"
titre_slider: ''
weight: 10

---
### _Les « sèche-pleurs » est le nom donné aux produits reconnus pour leur capacité à faire disparaître instantanément les gros chagrins des enfants._

#### Monoprix confie aux Bons Faiseurs la création d’une gamme de 53 produits au rayon jouet et goûter d’anniversaire.

#### Clin d’oeil à la bande dessinée, chaque objet est illustré par une onomatopée qui préfigure l’action de jeu qui lui est associé. Un territoire fort, joyeux et sympathique, à la personnalité unique qui réussit le pari de séduire les enfants sans être bêtifiant.