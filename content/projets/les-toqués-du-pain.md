---
title: Les Toqués du Pain ne pensent qu'à ça !
client: clients/les-toqués-du-pain.md
date: 2018-11-08 17:59:47 +0000
metiers:
- Branding
- Design global
prix: true
recompenses: A Design Award 2018 - Club des DA 2017 Typographie
slider: true
titre_slider: Identité visuelle
imageslider: "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_11.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_833x550_02.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_09.gif"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_02.jpg"
- template: ligne-12txt
  col12_txt: |-
    Quelques définitions

    Toqué, ée adj. et n. Un peu fou, bizarre, cinglé, timbré.

    Les TOC (Troubles Obsessionnels Compulsifs) sont des troubles anxieux qui se caractérisent par des idées obsédantes. Ces obsessions contraignent le sujet atteint à accomplir des compulsions (rituels ou pensées répétitives).
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_03.jpg"
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_1089x1650_01-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_1089x1650_02.jpg"
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_1089x1650_03.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_06.jpg"
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_07.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_08.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_10.jpg"
  - "/uploads/Les_Bons_Faiseurs_LES_TOQUES_DU_PAIN_2500x1650_09.jpg"
weight: 1

---
### Les Toqués du Pain sont mordus, passionnés, amoureux, accros, fous, obsessionnels et obnubilés par le pain.

### Ils pensent pain, ils vivent pain, ils rêvent pain sous toutes ses formes : en tartine, grillé, sucré, salé, en sandwich, en mouillette... Bref, ils souffrent de ce que nos experts nomment le TOC du pain.