---
title: Mon commerçant de quartier est un voisin formidable
client: clients/petit-casino.md
date: 2018-11-09 10:05:54 +0000
metiers:
- Retail
- Branding
- Design global
prix: true
recompenses: Janus du commerce 2018 - Typographie Club des AD 2017
slider: true
titre_slider: Concept retail
imageslider: "/uploads/Les_Bons_Faiseurs_VILLAMARX_06_2500x1650.jpg"
vignette: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_02_833x550-1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_VILLAMARX_05_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_01_06_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_06_960x1296.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_VILLAMARX_07_2500x1650.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_08.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_VILLAMARX_04_2500x1650.jpg"
- template: ligne-12txt
  col12_txt: |-
    #### _Le comptoir du Petit Casino_

    #### Très loin du passage en caisse technique, le comptoir est une nouvelle vision de la relation commerçante du petit Casino. Un lieu de convivialité, d'échange, de partage, de consommation et tout naturellement d'encaissement.
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_05_960x1296-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_05.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_07.jpg"
- template: ligne-4img-4img-4txt
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_06.jpg"
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_10_960x1296.jpg"
  col4_txt: |-
    ##### _Donner la parole à l'épicier._

    ##### À l'époque où on parle d'objets connectés, de magasins connectés, de frigos connectés... Nous avons fait le pari de l'humain connecté à l'humain. Un petit feutre blanc combiné à l'humeur du commerçant/voisin du Petit Casino. Place au sensible, au sincère, au coup de gueule, à l'urbanité (la vrai)...
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_02_06_2500x1650.jpg"
- template: ligne-8img-4img
  col8_img: "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_2500x1650_12.jpg"
  col4_img: "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_02_960x1296-2.jpg"
- template: ligne-8img-4txt
  col4_txt: |-
    ##### _L'art d'en faire des caisses._

    #### Faire d'une banale caisse de transport un outil de merchandising malin et économique. Elle devient pied de table d'arrivage, caisse à pomme, présentoir événementiel, bac à fleur... il y a mille raisons dans faire des caisses.
  col8_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_05_06_2500x1650-1.jpg"
- template: ligne-4img-4img-4txt
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_03-1.jpg"
  - "/uploads/Les_Bons_Faiseurs_LE_PETIT_CASINO_960x1296_04.jpg"
  col4_txt: |-
    ###### _Sur la musique de "avoir un bon copain"_

    ###### Avoir un bon voisin

    ###### Voilà c'qui y a d'meilleur au monde

    ###### Oui, car, un bon voisin

    ###### C'est plus beau qu'un hyper immonde

    ###### Tous les 4 matins

    ###### En une seconde

    ###### On trouve tous ses besoins

    ###### Quand on possède un bon voisin...
- template: ligne-12img
  col12_img: []
- template: ligne-4img-4img-4img
  col4_img:
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_20_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_PetitCasino_960x1296.jpg"
  - "/uploads/Les_Bons_Faiseurs_Le_Petit_Casino_15_06_2500x1650.jpg"
weight: 1

---
## Pionnière du commerce de proximité français depuis 1898, l’enseigne Petit Casino constitue l’ADN du groupe Casino. Pourtant au fil des années, elle avait perdu de son évidence. C’est en partant de ses racines historiques que Les Bons Faiseurs ont entrepris en 2017 de moderniser l'enseigne. Passer d'un lieu de dépannage à un lieu de vie. Aux antipodes de la tristounette supérette, Le Petit Casino est un lieu de sociabilité, où la relation commerçante est au coeur de l'attention.

## Chez les Bons Faiseurs nous croyons que c'est la qualité relationnelle du commerçant qui fait la différence.

## La petite attention, le bon produit du producteur du coin, l'actualité (voir les potins) du quartier... l'épicier du Petit Casino est votre plus précieux voisin.

#### Une nouvelle et jolie collaboration avec l'agence d'architecture Versions.