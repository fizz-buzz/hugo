---
title: El Sacramento
client: clients/el-sacramento.md
date: 2018-11-15 16:52:55 +0000
metiers:
- Packaging
- Branding
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: "/uploads/Les_Bons_Faiseurs_El_Sacramento_833x550-1.jpg"
ligne_col:
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_01_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_06_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_04_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_03_2500x1650.jpg"
- template: ligne-6img-6img
  col6_img:
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_08_2500x1650.jpg"
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_07_2500x1650.jpg"
- template: ligne-12img
  col12_img:
  - "/uploads/Les_Bons_Faiseurs_El_Sacramento_02_2500x1650.jpg"
weight: 3

---
## Le travail de la vigne est une des plus beaux métiers que l’homme puisse faire. D’ailleurs Étienne Cordonnier ne s’y est pas trompé. Après une belle carrière dans le commerce, c’est au cœur de la Roja, près de Laguardia, qu’il reprend des vignes. La plus belle parcelle porte le nom d’El Sacramento.

## C'est avec l'aide de Jacques Jubert un graveur talentueux que nous redessinons les armoiries familiales de la branche espagnole d’Étienne.

## _Los dichos en nos, los kochos en Dios._