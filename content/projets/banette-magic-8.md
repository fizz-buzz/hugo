---
title: Banette Magic !
client: clients/banette.md
date: 2018-11-11 13:59:02 +0000
metiers:
- Brand content
- Packaging
- Digital
prix: false
recompenses: ''
slider: false
titre_slider: ''
imageslider: ''
vignette: ''
ligne_col:
- template: ligne12video
  col12_video: https://player.vimeo.com/video/161591346
- template: ligne-12img
  col12_img:
  - "/uploads/giphy.gif"
weight: 10
draft: true

---
## **De la couleur, un peu de magie, c'est la vie !**

### Banette Magic est un dispositif qui donne une seconde vie aux emballages des pains en proposant des coloriages. Grâce à la technologie de la réalité augmentée, une application permet de donner vie à ces coloriages et de les partager sur Facebook et Twitter. Tout au long de l’année, les enfants retrouveront chez leurs artisans boulangers des nouveaux dessins _Banette Magic_.

### C'est cool, non ?

_

Créée par les Bons Faiseurs et réalisée par le studio Atomic Soom.